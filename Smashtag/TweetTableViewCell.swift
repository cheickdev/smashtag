//
//  TweetTableViewCell.swift
//  Smashtag
//
//  Created by Cheick Mahady SISSOKO on 02/01/2016.
//  Copyright © 2016 Telecom ParisTech. All rights reserved.
//

import UIKit

class TweetTableViewCell: UITableViewCell {

    var tweet: Tweet? {
        didSet {
            updateUI()
        }
    }

    @IBOutlet weak var tweetProfileImageView: UIImageView!
    @IBOutlet weak var tweetScreenNameLabel: UILabel!
    @IBOutlet weak var tweetTextLabel: UILabel!
    
    
    
    func updateUI() {
        tweetTextLabel.attributedText = nil
        tweetScreenNameLabel.text = nil
        tweetProfileImageView.image = nil
        
        // tweetCreateLabel.text = nil
        
        if let tweet = self.tweet {
            tweetTextLabel.text = tweet.text
            if tweetTextLabel.text != nil {
                for _ in tweet.media {
                    tweetTextLabel.text! += " 📷"
                }
            }
            
            tweetScreenNameLabel.text = "\(tweet.user)"
            
            if let profileImageURL = tweet.user.profileImageURL {
                let qos = Int(QOS_CLASS_USER_INITIATED.rawValue)
                dispatch_async(dispatch_get_global_queue(qos, 0), { () -> Void in
                    if let imageData = NSData(contentsOfURL: profileImageURL) {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.tweetProfileImageView.image = UIImage(data: imageData)
                        })
                    }
                })
                
            }
            
//            let formatter = NSDateFormatter()
//            if NSDate().timeIntervalSinceDate(tweet.created) > 24 * 60 * 60 {
//                formatter.dateStyle = NSDateFormatterStyle.ShortStyle
//            } else {
//                formatter.timeStyle = NSDateFormatterStyle.ShortStyle
//            }
//            
//            tweetCreateLabel.text = formatter.stringFromDate(tweet.created)
            
        }
    }
    
}
